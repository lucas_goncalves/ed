package ex1

import ex1.Hash

object Main {
  def main(args : Array[String]) : Unit = {
    var size : Int = 5
    var h = new Hash(size)
    h.addHash(1)
    h.addHash(2)
    h.addHash(3)
    h.addHash(4)
    h.addHash(5)
    h.addHash(6)
    println(h.findHash(6))
    println("----------------------")
    println(h.findHash(7))
    println("----------------------")
    h.printHash()
    println("----------------------")
    h.delHash(1)
    h.delHash(2)
    println("----------------------")
    h.printHash()
    println("----------------------")
    h.addHash(2)
    println("----------------------")
    h.printHash()
  }
}