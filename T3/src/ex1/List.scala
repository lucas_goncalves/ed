package ex1

class List(l:Node = null) {
  var node : Node = l
  
  def add(value : Int): Unit ={
    var n : Node = new Node(value,this.node)
    this.node = n
  }
  def imprime(): Unit ={
    var aux : Node = this.node
    while(aux != null){
      println(aux.getHead())
      aux = aux.getTail()
    }
  }
  def imprimeRec(): Unit ={
    var aux : Node = this.node
    rec(aux)
  }
  
  def rec(n : Node): Unit ={
    if(n!=null){
      println(n.getHead())
      rec(n.getTail())
    }
  }
  def imprimeInv(): Unit ={
    var aux : Node = this.node
    recInv(aux)
  }
  def recInv(n : Node): Unit ={
    if(n!=null){
      recInv(n.getTail())
      println(n.getHead())
    }
  }
  def isEmpty(): Boolean ={
    if(this.node==null){
      return true
    }
    else
      return false
  }
  def find(value : Int) : Node ={
    var aux : Node = this.node
    while(aux!=null){
      if(aux.getHead()==value) return aux
      else aux = aux.getTail()
    }
    return aux
  }
  def remove(value : Int) : Unit ={
    if(this.node.getHead()==value){
      this.node = this.node.getTail()
      return
    }
    var aux : Node = this.node
    while(aux!=null) {
      if(aux.getHead()==value){
        var rem : Node = this.node
        while(rem.getTail()!=aux){
          rem = rem.getTail()
        }
        rem.setTail(aux.getTail())
        return
      }
      aux = aux.getTail()
    }
  }
  def removeRec(value : Int) : Unit ={
    this.node = remRec(this.node, value)
  }
  def remRec(n : Node, value : Int) : Node ={
    if(n==null){
      return n
    }
    else{
      if(n.getHead()==value){
        return remRec(n.getTail(),value)
      }
      n.setTail(remRec(n.getTail(),value))
      return n
    }
  }
  def free() : Unit ={
    this.node = null
  }
}