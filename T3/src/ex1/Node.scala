package ex1

class Node(value : Int, next : Node) {
  var head : Int = value
  var tail : Node = next
  def getHead(): Int ={
    return this.head;
  }
  def setHead(value : Int): Unit ={
    this.head = value;
  }
  def getTail(): Node ={
    return this.tail;
  }
  def setTail(value : Node): Unit ={
    this.tail = value
  }
}