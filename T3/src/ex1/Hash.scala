package ex1

class Hash(n : Int) {
  var size = n
  var list = new Array[List](size)
  createHash()
  
  def func(n : Int) : Int = {
    return (n)%(this.size)
  }
  def createHash() : Unit ={
    var i : Int = 0
    for(i <- 1 to (this.size)){
    list(i-1)=new List()
    }
  }
  def addHash(num : Int) : Unit = {
    list(func(num)).add(num)
  }
  def findHash(num : Int) : Int = {
    var n : Node = list(func(num)).find(num)
    if(n != null) return n.getHead()
    else return -1
  }
  def delHash(num : Int) : Unit = {
    list(func(num)).remove(num)
  }
  def printHash() : Unit = {
    var n : Int = 0
    for(i <- 1 to (this.size)) {
      this.list(i-1).imprime()  
      println("----------------------")
    }
  }
  def freeHash() : Unit ={
    var i : Int = 0
    for(i <- 1 to (this.size)){
    list(i-1).free()
    }
  }
}