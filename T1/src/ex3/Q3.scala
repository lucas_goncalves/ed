package ex3

class Q3(l:List = null) {
  var node : List = l
  
  def add(value : Int): Unit ={
    if(this.node == null){
      var n : List = new List(value,this.node,null)
      this.node = n
      return
    }
    else {
      if(this.node.getHead()>=value){
        var aux : List = new List(value,this.node,null)
        this.node = aux
        return
      }
      var aux : List = this.node
      while(aux.getTail()!=null){
        if(aux.getHead()<value && aux.getTail().getHead()>=value){
          var n : List = aux.getTail()
          var novo : List = new List(value,aux.getTail(),aux)
          n.setHair(novo)
          aux.setTail(novo)
          return
        }
        aux = aux.getTail()
      }
      aux.setTail(new List(value,null,aux))
      return
    }
  }
  
  def imprime(): Unit ={
    var aux : List = this.node
    while(aux != null){
      println(aux.getHead())
      aux = aux.getTail()
    }
  }
  
  def imprimeRec(): Unit ={
    var aux : List = this.node
    rec(aux)
  }
  
  def rec(n : List): Unit ={
    if(n!=null){
      println(n.getHead())
      rec(n.getTail())
    }
  }
  
  def imprimeInv(): Unit ={
    var aux : List = this.node
    recInv(aux)
  }
  
  def recInv(n : List): Unit ={
    if(n!=null){
      recInv(n.getTail())
      println(n.getHead())
    }
  }
  
  def isEmpty(): Boolean ={
    if(this.node==null){
      return true
    }
    else
      return false
  }
  
  def find(value : Int) : List ={
    var aux : List = this.node
    while(aux!=null){
      if(aux.getHead()==value) return aux
      else aux = aux.getTail()
    }
    return aux
  }
  
  def remove(value : Int) : Unit ={
    if(this.node.getHead()==value){
      this.node = this.node.getTail()
      this.node.setHair(null)
      corrige()
      return
    }
    var aux : List = this.node
    while(aux!=null) {
      if(aux.getHead()==value){
        var rem : List = this.node
        while(rem.getTail()!=aux){
          rem = rem.getTail()
        }
        rem.setTail(aux.getTail())
        corrige()
        return
      }
      aux = aux.getTail()
    }
  }
  
  def removeRec(value : Int) : Unit ={
    this.node = remRec(this.node, value)
    corrige()
  }
  
  def remRec(n : List, value : Int) : List ={
    if(n==null){
      return n
    }
    else{
      if(n.getHead()==value){
        return remRec(n.getTail(),value)
      }
      n.setTail(remRec(n.getTail(),value))
      return n
    }
  }
  
  def corrige() : Unit ={
    var aux : List = this.node
    while(aux!=null){
      if(aux.getTail()!=null){
        aux.getTail().setHair(aux)
      }
      aux = aux.getTail()
    }
  }
  
  def free() : Unit ={
    this.node = null
  }
  
  def getSize() : Int ={
    var aux : List = this.node
    var i : Int = 0
    while(aux != null){
      i = i+1
      aux = aux.getTail()
    }
    return i
  }
  
  def equalsTo(l : Q3) : Boolean ={
    var aux1 = this.node
    var aux2 = l.node
    if(this.getSize() != l.getSize())
      return false
    while(aux1!=null){
      if(aux1.getHead()==aux2.getHead()){
        aux1 = aux1.getTail()
        aux2 = aux2.getTail()
      }
      else return false
    }
    return true
  }
}