package ex5

import ex5.Q5

object Main {
  def main(args: Array[String]): Unit = {
    var l : Q5 = new Q5()
    l.add(1)
    l.add(2)
    l.add(3)
    l.imprime()
    println("----------------")
    l.imprimeRec()
    println("----------------")
    l.imprimeInv()
    println("----------------")
    println(l.isEmpty())
    println("----------------")
    println(l.find(2))
    println(l.find(5))
    println("----------------")
    l.remove(1)
    l.imprime()
    println("----------------")
    l.removeRec(2)
    l.imprime()
    println("----------------")
    var l2 : Q5 = new Q5()
    println("----------------")
    println(l.equalsTo(l2))
    l2.add(2)
    println("----------------")
    println(l.equalsTo(l2))
    l2.add(3)
    println("----------------")
    println(l.equalsTo(l2))
    l2.remove(2)
    println("----------------")
    println(l.equalsTo(l2))
  }
}