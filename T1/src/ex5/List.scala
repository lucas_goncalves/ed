package ex5

class List(value : Int, next : List, previous : List) {
  var head : Int = value
  var tail : List = next
  var hair : List = previous
  var end : Boolean = _
  def getHead(): Int ={
    return this.head;
  }
  def setHead(value : Int): Unit ={
    this.head = value;
  }
  def getTail(): List ={
    return this.tail;
  }
  def setTail(value : List): Unit ={
    this.tail = value
  }
  def getHair(): List ={
    return this.hair;
  }
  def setHair(value : List): Unit ={
    this.hair = value
  }
  def isEnd() : Boolean ={
    return end
  }
  def setEnd(b : Boolean) : Unit ={
    this.end = b
  }
}