package ex6

class ContaBancaria(value : Int, next : ContaBancaria) {
  var numero : Int = value
  var saldo : Double = 0
  var tail : ContaBancaria = next
  def getNumero(): Int ={
    return this.numero;
  }
  def setNumero(value : Int): Unit ={
    this.numero = value;
  }
  def getTail(): ContaBancaria ={
    return this.tail;
  }
  def setTail(value : ContaBancaria): Unit ={
    this.tail = value
  }
  def getSaldo() : Double ={
    return this.saldo
  }
  def setSaldo(value : Double) : Unit ={
    this.saldo = value
  }
  def creditar(value : Double) : Unit ={
    this.setSaldo(this.getSaldo()+value)
  }
  def debitar(value : Double) : Unit ={
    if(this.getSaldo()-value < 0){
      println("Saldo insuficiente.")
    }
    else{
      this.setSaldo(this.getSaldo()-value)
    }
  }
}