package ex6

class Q6CP(l:ContaPoupanca = null) {
  var node : ContaPoupanca = l
  
  def add(value : Int): Unit ={
    var n : ContaPoupanca = new ContaPoupanca(value,this.node)
    this.node = n
  }
  def imprime(): Unit ={
    var aux : ContaPoupanca = this.node
    while(aux != null){
      println(aux.getNumero())
      aux = aux.getTail()
    }
  }
  def imprimeRec(): Unit ={
    var aux : ContaPoupanca = this.node
    rec(aux)
  }
  
  def rec(n : ContaPoupanca): Unit ={
    if(n!=null){
      println(n.getNumero())
      rec(n.getTail())
    }
  }
  def imprimeInv(): Unit ={
    var aux : ContaPoupanca = this.node
    recInv(aux)
  }
  def recInv(n : ContaPoupanca): Unit ={
    if(n!=null){
      recInv(n.getTail())
      println(n.getNumero())
    }
  }
  def isEmpty(): Boolean ={
    if(this.node==null){
      return true
    }
    else
      return false
  }
  def find(value : Int) : ContaPoupanca ={
    var aux : ContaPoupanca = this.node
    while(aux!=null){
      if(aux.getNumero()==value) return aux
      else aux = aux.getTail()
    }
    return aux
  }
  def remove(value : Int) : Unit ={
    if(this.node.getNumero()==value){
      this.node = this.node.getTail()
      return
    }
    var aux : ContaPoupanca = this.node
    while(aux!=null) {
      if(aux.getNumero()==value){
        var rem : ContaPoupanca = this.node
        while(rem.getTail()!=aux){
          rem = rem.getTail()
        }
        rem.setTail(aux.getTail())
        return
      }
      aux = aux.getTail()
    }
  }
  def removeRec(value : Int) : Unit ={
    this.node = remRec(this.node, value)
  }
  def remRec(n : ContaPoupanca, value : Int) : ContaPoupanca ={
    if(n==null){
      return n
    }
    else{
      if(n.getNumero()==value){
        return remRec(n.getTail(),value)
      }
      n.setTail(remRec(n.getTail(),value))
      return n
    }
  }
  def free() : Unit ={
    this.node = null
  }
}