package ex6

class ContaEspecial(value : Int, next : ContaEspecial) {
  var numero : Int = value
  var saldo : Double = 0
  var bonus : Double = 0
  var tail : ContaEspecial = next
  def getNumero(): Int ={
    return this.numero;
  }
  def setNumero(value : Int): Unit ={
    this.numero = value;
  }
  def getTail(): ContaEspecial ={
    return this.tail;
  }
  def setTail(value : ContaEspecial): Unit ={
    this.tail = value
  }
  def getSaldo() : Double ={
    return this.saldo
  }
  def setSaldo(value : Double) : Unit ={
    this.saldo = value
  }
  def getBonus() : Double ={
    return this.bonus
  }
  def setBonus(value : Double) : Unit ={
    this.bonus = value
  }
  def creditar(value : Double) : Unit ={
    this.setSaldo(this.getSaldo()+value)
    this.setBonus(this.getBonus()+(value * 1.01))
  }
  def debitar(value : Double) : Unit ={
    if(this.getSaldo()-value < 0){
      println("Saldo insuficiente.")
    }
    else{
      this.setSaldo(this.getSaldo()-value)
    }
  }
  def renderBonus() : Unit ={
    this.setSaldo(this.getSaldo()+this.getBonus())
    this.setBonus(0)
  }
}