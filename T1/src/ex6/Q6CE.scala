package ex6

class Q6CE(l:ContaEspecial = null) {
  var node : ContaEspecial = l
  
  def add(value : Int): Unit ={
    var n : ContaEspecial = new ContaEspecial(value,this.node)
    this.node = n
  }
  def imprime(): Unit ={
    var aux : ContaEspecial = this.node
    while(aux != null){
      println(aux.getNumero())
      aux = aux.getTail()
    }
  }
  def imprimeRec(): Unit ={
    var aux : ContaEspecial = this.node
    rec(aux)
  }
  
  def rec(n : ContaEspecial): Unit ={
    if(n!=null){
      println(n.getNumero())
      rec(n.getTail())
    }
  }
  def imprimeInv(): Unit ={
    var aux : ContaEspecial = this.node
    recInv(aux)
  }
  def recInv(n : ContaEspecial): Unit ={
    if(n!=null){
      recInv(n.getTail())
      println(n.getNumero())
    }
  }
  def isEmpty(): Boolean ={
    if(this.node==null){
      return true
    }
    else
      return false
  }
  def find(value : Int) : ContaEspecial ={
    var aux : ContaEspecial = this.node
    while(aux!=null){
      if(aux.getNumero()==value) return aux
      else aux = aux.getTail()
    }
    return aux
  }
  def remove(value : Int) : Unit ={
    if(this.node.getNumero()==value){
      this.node = this.node.getTail()
      return
    }
    var aux : ContaEspecial = this.node
    while(aux!=null) {
      if(aux.getNumero()==value){
        var rem : ContaEspecial = this.node
        while(rem.getTail()!=aux){
          rem = rem.getTail()
        }
        rem.setTail(aux.getTail())
        return
      }
      aux = aux.getTail()
    }
  }
  def removeRec(value : Int) : Unit ={
    this.node = remRec(this.node, value)
  }
  def remRec(n : ContaEspecial, value : Int) : ContaEspecial ={
    if(n==null){
      return n
    }
    else{
      if(n.getNumero()==value){
        return remRec(n.getTail(),value)
      }
      n.setTail(remRec(n.getTail(),value))
      return n
    }
  }
  def free() : Unit ={
    this.node = null
  }
}