package ex6

import ex6.Q6CB
import ex6.Q6CP
import ex6.Q6CE

object Main {
  def main(args: Array[String]): Unit = {
    var lb : Q6CB = new Q6CB()
    var lp : Q6CP = new Q6CP()
    var le : Q6CE = new Q6CE()
    lb.add(1)
    lb.add(4)
    lp.add(2)
    le.add(3)
    lb.node.creditar(10)
    lb.node.getTail().creditar(10)
    lp.node.creditar(10)
    le.node.creditar(10)
    lb.node.debitar(5)
    lb.node.getTail().debitar(5)
    lp.node.debitar(5)
    le.node.debitar(5)
    println(lb.node.getSaldo())
    println("-----------------------")
    println(lb.node.getTail().getSaldo())
    println("-----------------------")
    println(lp.node.getSaldo())
    println("-----------------------")
    println(le.node.getSaldo())
    println("-----------------------")
    println(le.node.getBonus())
    println("-----------------------")
    transferenciaCBCP(lb.node,lp.node,1)
    lp.node.renderJuros()
    le.node.renderBonus()
    lb.remove(1)
    println("-----------------------")
    listaCB(lb)
    println("-----------------------")
    listaCP(lp)
    println("-----------------------")
    listaCE(le)
    
  }
  def transferenciaCBCP(origem : ContaBancaria, destino : ContaPoupanca, value : Double) : Unit ={
    if(origem.getSaldo()-value<0){
      println("Saldo insuficiente")
    }
    else{
      origem.debitar(value)
      destino.creditar(value)
    }
  }
  def listaCB(l : Q6CB) : Unit = {
    var aux : ContaBancaria = l.node
    while(aux != null){
      println("Numero: "+aux.getNumero()+" ------ Saldo: "+aux.getSaldo())
      aux = aux.getTail()
    }
  }
  def listaCP(l : Q6CP) : Unit = {
    var aux : ContaPoupanca = l.node
    while(aux != null){
      println("Numero: "+aux.getNumero()+" ------ Saldo: "+aux.getSaldo())
      aux = aux.getTail()
    }
  }
  def listaCE(l : Q6CE) : Unit = {
    var aux : ContaEspecial = l.node
    while(aux != null){
      println("Numero: "+aux.getNumero()+" ------ Saldo: "+aux.getSaldo())
      aux = aux.getTail()
    }
  }
}