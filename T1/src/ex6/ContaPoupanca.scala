package ex6

class ContaPoupanca(value : Int, next : ContaPoupanca) {
  var numero : Int = value
  var saldo : Double = 0
  var tail : ContaPoupanca = next
  def getNumero(): Int ={
    return this.numero;
  }
  def setNumero(value : Int): Unit ={
    this.numero = value;
  }
  def getTail(): ContaPoupanca ={
    return this.tail;
  }
  def setTail(value : ContaPoupanca): Unit ={
    this.tail = value
  }
  def getSaldo() : Double ={
    return this.saldo
  }
  def setSaldo(value : Double) : Unit ={
    this.saldo = value
  }
  def creditar(value : Double) : Unit ={
    this.setSaldo(this.getSaldo()+value)
  }
  def debitar(value : Double) : Unit ={
    if(this.getSaldo()-value < 0){
      println("Saldo insuficiente.")
    }
    else{
      this.setSaldo(this.getSaldo()-value)
    }
  }
  def renderJuros() : Unit ={
    this.setSaldo(this.getSaldo() * 1.01)
  }
}