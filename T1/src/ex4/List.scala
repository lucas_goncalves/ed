package ex4

class List(value : Int, next : List) {
  var head : Int = value
  var end : Boolean = _
  var tail : List = next
  def getHead(): Int ={
    return this.head;
  }
  def setHead(value : Int): Unit ={
    this.head = value;
  }
  def getTail(): List ={
    return this.tail;
  }
  def setTail(value : List): Unit ={
    this.tail = value
  }
  def getEnd(): Boolean ={
    return this.end
  }
  def setEnd(b : Boolean): Unit ={
    this.end = b
  }
}