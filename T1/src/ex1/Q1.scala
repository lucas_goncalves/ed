package ex1

class Q1(l:List = null) {
  var node : List = l
  
  def add(value : Int): Unit ={
    var n : List = new List(value,this.node)
    this.node = n
  }
  def imprime(): Unit ={
    var aux : List = this.node
    while(aux != null){
      println(aux.getHead())
      aux = aux.getTail()
    }
  }
  def imprimeRec(): Unit ={
    var aux : List = this.node
    rec(aux)
  }
  
  def rec(n : List): Unit ={
    if(n!=null){
      println(n.getHead())
      rec(n.getTail())
    }
  }
  def imprimeInv(): Unit ={
    var aux : List = this.node
    recInv(aux)
  }
  def recInv(n : List): Unit ={
    if(n!=null){
      recInv(n.getTail())
      println(n.getHead())
    }
  }
  def isEmpty(): Boolean ={
    if(this.node==null){
      return true
    }
    else
      return false
  }
  def find(value : Int) : List ={
    var aux : List = this.node
    while(aux!=null){
      if(aux.getHead()==value) return aux
      else aux = aux.getTail()
    }
    return aux
  }
  def remove(value : Int) : Unit ={
    if(this.node.getHead()==value){
      this.node = this.node.getTail()
      return
    }
    var aux : List = this.node
    while(aux!=null) {
      if(aux.getHead()==value){
        var rem : List = this.node
        while(rem.getTail()!=aux){
          rem = rem.getTail()
        }
        rem.setTail(aux.getTail())
        return
      }
      aux = aux.getTail()
    }
  }
  def removeRec(value : Int) : Unit ={
    this.node = remRec(this.node, value)
  }
  def remRec(n : List, value : Int) : List ={
    if(n==null){
      return n
    }
    else{
      if(n.getHead()==value){
        return remRec(n.getTail(),value)
      }
      n.setTail(remRec(n.getTail(),value))
      return n
    }
  }
  def free() : Unit ={
    this.node = null
  }
}