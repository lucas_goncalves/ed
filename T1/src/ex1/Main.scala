package ex1

import ex1.Q1

object Main {
  def main(args: Array[String]): Unit = {
    var l : Q1 = new Q1()
    l.add(1)
    l.add(2)
    l.add(3)
    l.imprime()
    println("----------------")
    l.imprimeRec()
    println("----------------")
    l.imprimeInv()
    println("----------------")
    println(l.isEmpty())
    println("----------------")
    println(l.find(2))
    println(l.find(5))
    println("----------------")
    l.removeRec(3)
    l.remove(1)
    l.imprime()
    println("----------------")
    l.removeRec(2)
    l.imprime()
    println("----------------")
  }
}