package ex1

class List(value : Int, next : List) {
  var head : Int = value
  var tail : List = next
  def getHead(): Int ={
    return this.head;
  }
  def setHead(value : Int): Unit ={
    this.head = value;
  }
  def getTail(): List ={
    return this.tail;
  }
  def setTail(value : List): Unit ={
    this.tail = value
  }
}