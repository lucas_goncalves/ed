package ex1

object Main {
  def main(args : Array[String]) : Unit ={
    var set = new Vec(5)
    set.union(0, 1)
    set.union(1,2)
    set.union(2, 3)
    println(set.list(0).parent+" "+set.list(1).parent+" "+set.list(2).parent+" "+set.list(3).parent+" "+set.list(4).parent)
    println(set.find(0)+" "+set.find(1)+" "+set.find(2)+" "+set.find(3)+" "+set.find(4))
    println("--------------")
    println(set.isSame(0, 3)+" "+set.isSame(2, 4))
    println("--------------")
    set.free()
  }
}