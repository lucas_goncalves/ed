package ex1

class Vec(num : Int) {
  var tam = num
  var list : Array[Node] = new Array[Node](num)
  for(i <- 0 to num-1){
    list(i) = new Node()
  }
  
  def find(v : Int) : Int ={
    var ret : Int = v
    while(list(ret).parent != -1) 
      ret = list(ret).parent
    return ret
  }
  
  def union(e1 : Int, e2 : Int) : Unit ={
    var v1 = find(e1)
    var v2 = find(e2)
    list(v2).parent = v1
  }
  
  def isSame(e1 : Int, e2 : Int) : Boolean ={
    if(find(e1)==find(e2)) 
      return true
    else
      return false
  }
  
  def free() : Unit ={
    list = null
  }
}