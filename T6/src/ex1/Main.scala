package ex1

object Main {
  def main(args : Array[String]) : Unit ={
      var S1 = new Set(10)
      var S2 = new Set(12)
      S1.insert(1)
      S1.insert(2)
      S1.insert(3)
      println(S1.isIn(1)+" "+S1.isIn(2)+" "+S1.isIn(3)+" "+S1.isIn(4))
      println("-------------------------")
      S1.remove(3)
      println(S1.isIn(1)+" "+S1.isIn(2)+" "+S1.isIn(3)+" "+S1.isIn(4))
      println("-------------------------")
      S2.insert(2)
      S2.insert(4)
      println(S2.isIn(1)+" "+S2.isIn(2)+" "+S2.isIn(3)+" "+S2.isIn(4))
      println("-------------------------")
      var S = S1.union(S2)
      println(S.isIn(1)+" "+S.isIn(2)+" "+S.isIn(3)+" "+S.isIn(4))
      println("-------------------------")
      S = S1.inter(S2)
      println(S.isIn(1)+" "+S.isIn(2)+" "+S.isIn(3)+" "+S.isIn(4))
      println("-------------------------")
      S = S1.dif(S2)
      println(S.isIn(1)+" "+S.isIn(2)+" "+S.isIn(3)+" "+S.isIn(4))
      println("-------------------------")
      S2.remove(4)
      S = S2
      println(S1.sub(S)+" "+S.sub(S1))
      println("-------------------------")
      S = new Set(12)
      S.insert(1)
      S.insert(2)
      println(S1.eq(S)+" "+S2.eq(S1))
      println("-------------------------")
      S = S1.neg()
      for(i <- 1 to S1.size){
        print(S1.isIn(i)+" ")
      }
      println("")
      for(i <- 1 to S.size){
        print(S.isIn(i)+" ")
      }
      println("")
      println("-------------------------")
      println(S1.isIn(1)+" "+S1.isIn(3))
      println("-------------------------")
      println(S1.getSize())
      println("-------------------------")
      S1.free()
      println(S1.getSize())
  }
}