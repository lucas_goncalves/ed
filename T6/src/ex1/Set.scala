package ex1

class Set(v : Int) {
  var s : Array[Boolean] = new Array[Boolean](v)
  var size : Int = v
  for(i <- 0 to v-1){
    s(i) = false
  }
  
  def insert(n : Int) : Unit ={
    if(n-1 > this.size) println("Elemento superior ao maximo do conjunto!")
    else this.s(n-1) = true
  }
  
  def remove(n : Int) : Unit ={
    if(n-1 > this.size) println("Elemento superior ao maximo do conjunto!")
    else this.s(n-1) = false
  }
  
  def union(S : Set) : Set ={
    var ret : Set = null
    if(S.size > this.size) {
       ret = new Set(S.size)
       for(i <- 0 to this.size-1){
         if(S.s(i)==true | this.s(i)==true) ret.s(i) = true
         else ret.s(i) = false
       }
       for(j <- this.size to S.size-1) ret.s(j)=S.s(j)
       return ret
    }
    else{
       ret = new Set(this.size)
       for(i <- 0 to S.size-1){
         if(S.s(i)==true | this.s(i)==true) ret.s(i) = true
         else ret.s(i) = false
       }
       for(j <- S.size to this.size-1) ret.s(j)=this.s(j)
       return ret
    } 
  }
  
  def inter(S : Set) : Set ={
    var ret : Set = null
    if(S.size > this.size) {
       ret = new Set(S.size)
       for(i <- 0 to this.size-1){
         if(S.s(i)==true & this.s(i)==true) ret.s(i) = true
         else ret.s(i) = false
       }
       for(j <- this.size to S.size-1) ret.s(j)=S.s(j)
       return ret
    }
    else{
       ret = new Set(this.size)
       for(i <- 0 to S.size-1){
         if(S.s(i)==true & this.s(i)==true) ret.s(i) = true
         else ret.s(i) = false
       }
       for(j <- S.size to this.size-1) ret.s(j)=this.s(j)
       return ret
    } 
  }
  
  def dif(S : Set) : Set ={
    var ret : Set = null
    if(S.size > this.size) {
       ret = new Set(S.size)
       for(i <- 0 to this.size-1){
         if(S.s(i)==true & this.s(i)==true) ret.s(i) = false
         else ret.s(i) = this.s(i)
       }
       for(j <- this.size to S.size-1) ret.s(j)=S.s(j)
       return ret
    }
    else{
       ret = new Set(this.size)
       for(i <- 0 to S.size-1){
         if(S.s(i)==true & this.s(i)==true) ret.s(i) = false
         else ret.s(i) = this.s(i)
       }
       for(j <- S.size to this.size-1) ret.s(j)=this.s(j)
       return ret
    } 
  }
  
  def sub(S : Set) : Boolean ={
    if(this.getSize() > S.getSize()) return false
    else{
      var aux : Int = 0
      if(this.size < S.size) aux = this.size
      else aux = S.size
      for(i <- 0 to aux-1){
        if((this.s(i) == true) & (S.s(i) == false)) return false
      }
      if(this.size > S.size){
        for(i <- aux to this.size-1){
          if(this.s(i)==true) return false
        }
      }
    }
    return true
  }
  
  def eq(S : Set) : Boolean ={
    if(this.getSize() != S.getSize()) return false
    else{
      var aux : Int = 0
      if(this.size < S.size) aux = this.size
      else aux = S.size
      for(i <- 0 to aux-1){
        if(this.s(i)!=S.s(i)) return false
      }
      if(this.size > S.size){
        for(i <- aux to this.size-1){
          if(this.s(i)==true) return false
        }
      }
      if(S.size > this.size){
        for(i <- aux to S.size-1){
          if(S.s(i)==true) return false
        }
      }
    }
    return true
  }
  
  def neg() : Set ={
    val S = new Set(this.size)
    for(i <- 0 to this.size-1){
      S.s(i) = !this.s(i)
    }
    return S
  }
  
  def isIn(n : Int) : Boolean ={
    if(n-1 > this.size) return false
    else{
      return this.s(n-1)
    }
  }
  
  def getSize() : Int ={
    var ret : Int = 0
    for(i <- 0 to this.size-1){
      if(this.s(i)==true) ret = ret + 1
    }
    return ret
  }
  
  def free() : Unit ={
    this.s = null
    this.size = 0
  }
}