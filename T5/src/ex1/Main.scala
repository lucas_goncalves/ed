package ex1
import ex1.MinHeap

object Main {
  def main(args: Array[String]) : Unit = {
    var heap = new MinHeap(10)
    heap.add(3)
    println(heap.peek())
    heap.add(2)
    println(heap.peek())
    heap.add(1)
    println(heap.peek())
    heap.update(3, 0)
    println(heap.peek())
    heap.remove()
    println(heap.peek())
    heap.remove()
    println(heap.peek())
    heap.remove()
    println(heap.peek())
    heap.add(3)
    println(heap.peek())
    heap.add(2)
    println(heap.peek())
    heap.add(1)
    println(heap.peek())
    heap.free()
    println(heap.peek())
  }
}