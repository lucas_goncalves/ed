package ex1

class MinHeap(tam : Int) {
  var h : Array[Int] = new Array[Int](tam)
  var max = tam
  var pos = 0
  
  def add(v : Int) : Unit = {
    if(this.pos <= this.max-1){
      this.h(pos) = v
      pos = pos + 1
      correctUp(pos-1)
    }
    else{
      println("MinHeap cheio!")
    }
  }
  
  def change(heap : MinHeap, v1 : Int, v2 : Int) : MinHeap = {
    var aux : Int = 0
    aux = heap.h(v1)
    heap.h(v1) = heap.h(v2)
    heap.h(v2) = aux
    return heap
  }
  
  def correctUp(position : Int) : Unit = {
    var p : Int = position
    var parent : Int = 0
    while(p > 0) {
      parent = (p-1)/2
      if(this.h(p) < this.h(parent)){
        change(this,p,parent)
      }
      p = parent
    }
  }
  
  def correctDown(v : Int) : Unit = {
    var i = v
    var esq : Int = 0
    var dir : Int = 0
    var offspring : Int = 0
    while((2*i)+1 < this.pos){
      esq = (i*2)+1
      dir = (i*2)+2
      if(dir > pos) dir = esq
      if(this.h(dir)>this.h(esq)) offspring = esq
      else offspring = dir
      if(this.h(offspring) < this.h(i)){
        change(this,i,offspring)
      }
      i = offspring
    }
  }
  
  def peek() : Int = {
    if(pos>0){
      return this.h(0)
    }
    else{
      println("Heap vazio!")
      return -1
    }
  }
  
  def remove() : Int = {
    if(this.pos > 0){
      val r : Int = this.h(0)
      this.h(0) = this.h(this.pos-1)
      pos = pos-1
      correctDown(0)
      return r
    }
    else{
      println("MinHeap vazio!")
      return -1
    }
  }
  
  def update(old : Int, value : Int) : Unit ={
    for(i <- 0 to this.pos){
      if(this.h(i)==old){
        this.h(i) = value
        if(old < value){
          correctDown(i)
        }
        else{
          correctUp(i)
        }
      }
    }
  }
  def free() : Unit = {
    this.pos = 0
  }
}